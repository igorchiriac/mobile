const Lab = require('lab');
const Code = require('code');
const Path = require('path');
const Hapi = require('hapi');

// Test files must require the lab module, and export a test script
const lab = (exports.lab = Lab.script());

// shortcuts from lab
const { describe, it } = lab;

const expect = Code.expect;

describe('test car route', () => {
  it('should return validation error', async () => {
    const server = new Hapi.Server();

    // wait for the response and the request to finish
    const response = await server.inject('api/cars/te');
    expect(response.statusCode).to.equal(400);
  });
});

describe('test not fount route', () => {
  it('should return 404 status code for not found route', async () => {
    const server = new Hapi.Server();

    // wait for the response and the request to finish
    const response = await server.inject('/request-path');
    expect(response.statusCode).to.equal(404);
  });
});
