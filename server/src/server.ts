import * as Hapi from 'hapi';
import { IPlugin } from './plugins/interfaces';
import { IServerConfigurations } from './configurations';
import * as Cars from './cars';

export async function init(configs: IServerConfigurations): Promise<Hapi.Server> {
  try {
    const port = process.env.PORT || configs.port;
    const host = process.env.HOST || configs.host;
    const server = new Hapi.Server({
      port: port,
      host: host,
      routes: {
        cors: { origin: ['*'] },
      },
    });

    if (configs.routePrefix) {
      server.realm.modifiers.route.prefix = configs.routePrefix;
    }

    //  Setup Hapi Plugins
    const plugins: Array<string> = configs.plugins;
    const pluginOptions = {
      serverConfigs: configs,
    };

    let pluginPromises: Promise<any>[] = [];

    plugins.forEach((pluginName: string) => {
      const plugin: IPlugin = require('./plugins/' + pluginName).default();
      console.log(`Register Plugin ${plugin.info().name} v${plugin.info().version}`);
      pluginPromises.push(plugin.register(server, pluginOptions));
    });

    await Promise.all(pluginPromises);

    console.log('All plugins registered successfully.');

    Cars.init(server, configs);

    console.log('Routes registered successfully.');

    return server;
  } catch (err) {
    console.log('Error starting server: ', err);
    throw err;
  }
}
