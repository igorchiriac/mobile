import { IPlugin } from '../interfaces';
import * as Hapi from 'hapi';

const register = async (server: Hapi.Server): Promise<void> => {
  try {
    return server.register({
      plugin: require('blipp'),
    });
  } catch (err) {
    console.log(`Error registering blipp plugin: ${err}`);
    throw err;
  }
};

export default (): IPlugin => {
  return {
    register,
    info: () => {
      return {
        name: 'Blipp',
        version: '1.0.0',
      };
    },
  };
};
