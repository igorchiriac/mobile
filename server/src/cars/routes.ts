import * as Hapi from 'hapi';
import * as Joi from 'joi';
import CarController from './car-controller';
import { IServerConfigurations } from '../configurations';

export default function(server: Hapi.Server, configs: IServerConfigurations) {
  const carController = new CarController(configs);
  server.bind(carController);

  server.route({
    method: 'GET',
    path: '/cars/{carId}',
    config: {
      handler: carController.getCarById,
      tags: ['api', 'cars'],
      description: 'Get car by car id.',
      validate: {
        params: {
          carId: Joi.string()
            .required()
            .min(3),
        },
      },
      plugins: {
        'hapi-swagger': {
          responses: {
            '200': {
              description: 'Car found.',
            },
            '404': {
              description: 'Car with current Id does not exists.',
            },
            '500': {
              description: 'Internal server error.',
            },
          },
        },
      },
    },
  });
}
