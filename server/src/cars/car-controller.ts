import * as Hapi from 'hapi';
import * as Boom from 'boom';
import { IServerConfigurations } from '../configurations';
const carData = require('../car.json');

import axios from 'axios';

export default class CarController {
  private configs: IServerConfigurations;

  constructor(configs: IServerConfigurations) {
    this.configs = configs;
  }

  public async getCarById(request: Hapi.Request, h: Hapi.ResponseToolkit) {
    let carId = request.params['carId'];

    try {
      return h.response({ car: carData }).code(200);
    } catch (error) {
      return Boom.badImplementation(error);
    }
  }
}
