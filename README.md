Example of small app build with hapi js and react js.
Where server is a HapiJs implementation and it expose a get cars by id route to
the client. Client represents a react single page application which fetch the json
data and display the images in case of success or error message.

To run the application run `npm i-all` in the root folder. And after run
`npm run start` to start the client and server app concurently.
