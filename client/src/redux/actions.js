import {
  REQUEST_CAR,
  RECEIVE_CAR,
  SET_PICTURE,
  SET_API_ERROR,
  LOADING_INDICATOR,
} from './actionTypes';

import { getCarData } from '../api';

export function itemsAreLoading(isLoading) {
  return {
    type: LOADING_INDICATOR,
    payload: isLoading,
  };
}

export function requestCar() {
  return {
    type: REQUEST_CAR,
  };
}

export function receiveCar(payload) {
  return {
    type: RECEIVE_CAR,
    payload,
  };
}

export function setPicture(payload) {
  return {
    type: SET_PICTURE,
    payload,
  };
}

export function setError(error) {
  return {
    type: SET_API_ERROR,
    payload: error,
  };
}

export function carsFetchData(carId) {
  return dispatch => {
    dispatch(itemsAreLoading(true));
    getCarData(carId)
      .then(response => {
        if (response.status !== 200) {
          throw Error(response.statusText);
        }
        dispatch(itemsAreLoading(false));
        return response;
      })
      .then(response => dispatch(receiveCar(response.data.car)))
      .catch(() => dispatch(setError(true)));
  };
}
