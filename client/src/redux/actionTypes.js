export const REQUEST_CAR = '@app/REQUEST_CAR';
export const RECEIVE_CAR = '@app/RECEIVE_CAR';
export const SET_PICTURE = '@app/SET_PICTURE';
export const SET_API_ERROR = '@app/SET_API_ERROR';
export const LOADING_INDICATOR = '@app/LOADING_INDICATOR';
