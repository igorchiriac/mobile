import {
  RECEIVE_CAR,
  REQUEST_CAR,
  SET_API_ERROR,
  SET_PICTURE,
  LOADING_INDICATOR,
} from './actionTypes';

function rootReducer(
  state = {
    isFetching: false,
    car: null,
    selectedPicture: null,
    error: null,
  },
  action
) {
  switch (action.type) {
    case REQUEST_CAR:
      return Object.assign({}, state, {
        error: null,
      });
    case LOADING_INDICATOR:
      return Object.assign({}, state, {
        isFetching: action.payload,
      });
    case RECEIVE_CAR:
      return Object.assign({}, state, {
        isFetching: false,
        car: action.payload,
      });
    case SET_PICTURE:
      return Object.assign({}, state, {
        selectedPicture: action.payload,
      });
    case SET_API_ERROR:
      return Object.assign({}, state, {
        error: action.payload,
        isFetching: false,
      });
    default:
      return state;
  }
}

export default rootReducer;
