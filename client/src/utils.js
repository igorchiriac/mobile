// api returns base url, by appending _2 the small image is returned
// by appending _27 the big image is returned

export function createThumbnail(uri) {
  if (uri === null || uri === undefined)
    throw new Error('uri should be a string');
  return `https://${uri}_2.jpg`;
}
export function createLargePhoto(uri) {
  if (uri === null || uri === undefined)
    throw new Error('uri should be a string');
  return `https://${uri}_27.jpg`;
}
