import axios from 'axios';

export function getCarData(carId) {
  return axios.get(`http://0.0.0.0:5000/api/cars/${carId}`);
}
