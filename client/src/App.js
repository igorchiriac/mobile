import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import CarPage from './pages/Car';
import PhotoPage from './pages/Photo';

import { Provider } from 'react-redux';
import configureStore from './redux/configureStore';

import './styles/index.scss';

const store = configureStore();

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div>
            <Route exact path="/car" component={CarPage} />
            <Route exact path="/photo" component={PhotoPage} />
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
