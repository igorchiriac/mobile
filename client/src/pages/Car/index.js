import React from 'react';
import { connect } from 'react-redux';
import { carsFetchData, setPicture } from '../../redux/actions';
import './index.scss';
import Gallery from '../../components/Gallery';
import Loader from 'react-loader-spinner';

export class CarPage extends React.Component {
  componentDidMount() {
    console.log(this.props);
    this.props.carsFetchData(12345);
  }

  onNavigate = data => {
    this.props.setPicture(data);
    this.props.history.push('/photo');
  };

  renderCarsImages() {
    if (this.props.car) {
      return (
        <Gallery
          car={this.props.car}
          images={this.props.car.images}
          onNavigate={this.onNavigate}
        />
      );
    } else {
      return null;
    }
  }

  renderLoader() {
    return this.props.isFetching ? (
      <div className="Loader">
        <div className="Loader__center">
          <Loader type="Oval" color="#7fffd4" height={150} width={150} />
        </div>
      </div>
    ) : null;
  }

  renderError() {
    return this.props.error ? (
      <div className="Error">
        <span>An error occurred. Please refresh the page.</span>
      </div>
    ) : null;
  }

  render() {
    return (
      <div>
        <div className="Title">
          <span className="Title__text">Car Photos</span>
        </div>
        {this.renderLoader()}
        {this.renderError()}
        {this.renderCarsImages()}
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { car, isFetching, error } = state;
  return { car, isFetching, error };
}

const mapDispatchToProps = dispatch => {
  return {
    carsFetchData: carId => dispatch(carsFetchData(carId)),
    setPicture: data => dispatch(setPicture(data)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CarPage);
