import React from 'react';
import { connect } from 'react-redux';
import { createLargePhoto } from '../../utils';
import './index.scss';

class PhotoPage extends React.Component {
  componentWillMount() {
    if (!this.props.selectedPicture) {
      this.props.history.push('car');
    }
  }
  navigateToGallery = () => this.props.history.push('car');

  render() {
    return (
      <div>
        <div className="PhotoTitle">
          <div className="PhotoTitle__inline">
            <span>{this.props.car ? this.props.car.title : null}</span>
            <button
              className="PhotoTitle__close"
              onClick={this.navigateToGallery}
            />
          </div>
        </div>
        <div className="PhotoWrapper">
          {this.props.selectedPicture && (
            <img src={createLargePhoto(this.props.selectedPicture.uri)} />
          )}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { selectedPicture, car } = state;
  return { selectedPicture, car };
}

export default connect(mapStateToProps)(PhotoPage);
