const utils = require('../utils');

describe('createThumbnail function', function() {
  test('return correct url for thumbnail', () => {
    expect(utils.createThumbnail('test_uri')).toBe('https://test_uri_2.jpg');
  });

  test('logs an error when uri not defined', () => {
    expect(() => {
      utils.createThumbnail();
    }).toThrow('uri should be a string');
  });

  test('logs an error when uri is null', () => {
    expect(() => {
      utils.createThumbnail(null);
    }).toThrow('uri should be a string');
  });
});

describe('createThumbnail function', function() {
  test('return correct url for large photo', () => {
    expect(utils.createLargePhoto('test_uri')).toBe('https://test_uri_27.jpg');
  });

  test('logs an error when uri not defined', () => {
    expect(() => {
      utils.createLargePhoto();
    }).toThrow('uri should be a string');
  });

  test('logs an error when uri is null', () => {
    expect(() => {
      utils.createLargePhoto(null);
    }).toThrow('uri should be a string');
  });
});
