import React from 'react';
import ReactDOM from 'react-dom';
import Gallery from '../components/Gallery';
import { shallow } from 'enzyme';

describe('<Gallery/>', function() {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<Gallery images={[]} />, div);
  });
});

describe('render', () => {
  it('should render one image in gallery', () => {
    const gallery = shallow(
      <Gallery
        images={[
          {
            uri: 'i.ebayimg.com/00/s/NjgyWDEwMjQ=/z/znIAAOxynwlTcg-F/$',
          },
        ]}
        car={{ title: 'test' }}
      />
    );
    expect(gallery.find('img')).toHaveLength(1);
  });
});
