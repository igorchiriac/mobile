import React from 'react';
import { createThumbnail } from '../utils';

const Gallery = props => {
  return (
    <div className="Gallery">
      {props.images.map((image, index) => {
        return (
          <img
            key={index}
            src={createThumbnail(image.uri)}
            alt={props.car.title}
            onClick={() => props.onNavigate(image)}
          />
        );
      })}
    </div>
  );
};

export default Gallery;
